using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using LearnC.Data;
using LearnC.Models;

namespace LearnC.Pages.LearnCAdmin
{
    public class IndexModel : PageModel
    {
        private readonly LearnC.Data.LearnCContext _context;

        public IndexModel(LearnC.Data.LearnCContext context)
        {
            _context = context;
        }

        public IList<News> News { get;set; }

        public async Task OnGetAsync()
        {
            News = await _context.newsList
                .Include(n => n.learn).ToListAsync();
        }
    }
}
