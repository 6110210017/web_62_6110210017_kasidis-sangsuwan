﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using LearnC.Data;
using LearnC.Models;

namespace LearnC.Pages.LearnCAdmin
{
    public class CreateModel : PageModel
    {
        private readonly LearnC.Data.LearnCContext _context;

        public CreateModel(LearnC.Data.LearnCContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["LearnID"] = new SelectList(_context.Learn, "LearnID", "ShortName");
            return Page();
        }

        [BindProperty]
        public News News { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.newsList.Add(News);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}