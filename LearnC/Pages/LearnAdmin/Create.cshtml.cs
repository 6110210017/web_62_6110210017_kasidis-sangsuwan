using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using LearnC.Data;
using LearnC.Models;

namespace LearnC.Pages.LearnAdmin
{
    public class CreateModel : PageModel
    {
        private readonly LearnC.Data.LearnCContext _context;

        public CreateModel(LearnC.Data.LearnCContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Learn Learn { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Learn.Add(Learn);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}