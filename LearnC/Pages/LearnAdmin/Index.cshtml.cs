using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using LearnC.Data;
using LearnC.Models;

namespace LearnC.Pages.LearnAdmin
{
    public class IndexModel : PageModel
    {
        private readonly LearnC.Data.LearnCContext _context;

        public IndexModel(LearnC.Data.LearnCContext context)
        {
            _context = context;
        }

        public IList<Learn> Learn { get;set; }

        public async Task OnGetAsync()
        {
            Learn = await _context.Learn.ToListAsync();
        }
    }
}
