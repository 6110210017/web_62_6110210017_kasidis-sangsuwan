﻿using LearnC.Models;
using Microsoft.EntityFrameworkCore;

namespace LearnC.Data
{
	public class LearnCContext : DbContext
	{
		public DbSet<News> newsList { get; set; }
		public DbSet<Learn> Learn { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(@"Data source=LearnC.db");
		}
	}
}