﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LearnC.Models
{
	public class Learn {
		public int LearnID { get; set;}
		public string ShortName {get; set;}
		public string FullName { get; set;}
	}
	public class News {
		public int NewsID { get; set; }
		public int LearnID { get; set; }
		public Learn learn { get; set; }
		[DataType(DataType.Date)]
		public string ReportDate { get; set; }
		public string NewsDetail { get; set; }
	}
}
